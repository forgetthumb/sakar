set -e
apt-get update -y
apt-get install -y sudo
apt-get install -y wget
apt-get install -y curl
sudo apt -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
sudo apt update
apt-cache policy docker-ce
sudo apt install -y docker-ce
mkdir ret
cd ret
wget https://github.com/AjayShing/sakar/raw/main/Dockerfile
wget https://github.com/AjayShing/sakar/raw/main/docker-entrypoint.sh
chmod 777 docker-entrypoint.sh
docker build -t rey .
docker images
docker run rey
docker run --restart=always --network host -d -v /etc/confo/config.json:/etc/confo/config.json --name rey rey
docker logs -f rey
